import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('Echo e2e test', () => {

    let navBarPage: NavBarPage;
    let echoDialogPage: EchoDialogPage;
    let echoComponentsPage: EchoComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Echoes', () => {
        navBarPage.goToEntity('echo');
        echoComponentsPage = new EchoComponentsPage();
        expect(echoComponentsPage.getTitle())
            .toMatch(/jhechoApp.echo.home.title/);

    });

    it('should load create Echo dialog', () => {
        echoComponentsPage.clickOnCreateButton();
        echoDialogPage = new EchoDialogPage();
        expect(echoDialogPage.getModalTitle())
            .toMatch(/jhechoApp.echo.home.createOrEditLabel/);
        echoDialogPage.close();
    });

    it('should create and save Echoes', () => {
        echoComponentsPage.clickOnCreateButton();
        echoDialogPage.setMyEchoInput('myEcho');
        expect(echoDialogPage.getMyEchoInput()).toMatch('myEcho');
        echoDialogPage.save();
        expect(echoDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class EchoComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-echo div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class EchoDialogPage {
    modalTitle = element(by.css('h4#myEchoLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    myEchoInput = element(by.css('input#field_myEcho'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setMyEchoInput = function(myEcho) {
        this.myEchoInput.sendKeys(myEcho);
    };

    getMyEchoInput = function() {
        return this.myEchoInput.getAttribute('value');
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
