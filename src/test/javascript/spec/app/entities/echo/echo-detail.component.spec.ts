/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { JhechoTestModule } from '../../../test.module';
import { EchoDetailComponent } from '../../../../../../main/webapp/app/entities/echo/echo-detail.component';
import { EchoService } from '../../../../../../main/webapp/app/entities/echo/echo.service';
import { Echo } from '../../../../../../main/webapp/app/entities/echo/echo.model';

describe('Component Tests', () => {

    describe('Echo Management Detail Component', () => {
        let comp: EchoDetailComponent;
        let fixture: ComponentFixture<EchoDetailComponent>;
        let service: EchoService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JhechoTestModule],
                declarations: [EchoDetailComponent],
                providers: [
                    EchoService
                ]
            })
            .overrideTemplate(EchoDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(EchoDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EchoService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Echo(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.echo).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
