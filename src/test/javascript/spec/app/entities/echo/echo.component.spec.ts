/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { JhechoTestModule } from '../../../test.module';
import { EchoComponent } from '../../../../../../main/webapp/app/entities/echo/echo.component';
import { EchoService } from '../../../../../../main/webapp/app/entities/echo/echo.service';
import { Echo } from '../../../../../../main/webapp/app/entities/echo/echo.model';

describe('Component Tests', () => {

    describe('Echo Management Component', () => {
        let comp: EchoComponent;
        let fixture: ComponentFixture<EchoComponent>;
        let service: EchoService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [JhechoTestModule],
                declarations: [EchoComponent],
                providers: [
                    EchoService
                ]
            })
            .overrideTemplate(EchoComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(EchoComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EchoService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Echo(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.echoes[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
