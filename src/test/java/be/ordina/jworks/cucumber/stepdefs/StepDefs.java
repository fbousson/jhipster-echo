package be.ordina.jworks.cucumber.stepdefs;

import be.ordina.jworks.JhechoApp;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import org.springframework.boot.test.context.SpringBootTest;

@WebAppConfiguration
@SpringBootTest
@ContextConfiguration(classes = JhechoApp.class)
public abstract class StepDefs {

    protected ResultActions actions;

}
