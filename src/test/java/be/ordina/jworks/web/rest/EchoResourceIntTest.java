package be.ordina.jworks.web.rest;

import be.ordina.jworks.JhechoApp;

import be.ordina.jworks.domain.Echo;
import be.ordina.jworks.repository.EchoRepository;
import be.ordina.jworks.service.EchoService;
import be.ordina.jworks.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static be.ordina.jworks.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the EchoResource REST controller.
 *
 * @see EchoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = JhechoApp.class)
public class EchoResourceIntTest {

    private static final String DEFAULT_MY_ECHO = "AAAAAAAAAA";
    private static final String UPDATED_MY_ECHO = "BBBBBBBBBB";

    @Autowired
    private EchoRepository echoRepository;

    @Autowired
    private EchoService echoService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restEchoMockMvc;

    private Echo echo;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final EchoResource echoResource = new EchoResource(echoService);
        this.restEchoMockMvc = MockMvcBuilders.standaloneSetup(echoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Echo createEntity(EntityManager em) {
        Echo echo = new Echo()
            .myEcho(DEFAULT_MY_ECHO);
        return echo;
    }

    @Before
    public void initTest() {
        echo = createEntity(em);
    }

    @Test
    @Transactional
    public void createEcho() throws Exception {
        int databaseSizeBeforeCreate = echoRepository.findAll().size();

        // Create the Echo
        restEchoMockMvc.perform(post("/api/echoes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(echo)))
            .andExpect(status().isCreated());

        // Validate the Echo in the database
        List<Echo> echoList = echoRepository.findAll();
        assertThat(echoList).hasSize(databaseSizeBeforeCreate + 1);
        Echo testEcho = echoList.get(echoList.size() - 1);
        assertThat(testEcho.getMyEcho()).isEqualTo(DEFAULT_MY_ECHO);
    }

    @Test
    @Transactional
    public void createEchoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = echoRepository.findAll().size();

        // Create the Echo with an existing ID
        echo.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restEchoMockMvc.perform(post("/api/echoes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(echo)))
            .andExpect(status().isBadRequest());

        // Validate the Echo in the database
        List<Echo> echoList = echoRepository.findAll();
        assertThat(echoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllEchoes() throws Exception {
        // Initialize the database
        echoRepository.saveAndFlush(echo);

        // Get all the echoList
        restEchoMockMvc.perform(get("/api/echoes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(echo.getId().intValue())))
            .andExpect(jsonPath("$.[*].myEcho").value(hasItem(DEFAULT_MY_ECHO.toString())));
    }

    @Test
    @Transactional
    public void getEcho() throws Exception {
        // Initialize the database
        echoRepository.saveAndFlush(echo);

        // Get the echo
        restEchoMockMvc.perform(get("/api/echoes/{id}", echo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(echo.getId().intValue()))
            .andExpect(jsonPath("$.myEcho").value(DEFAULT_MY_ECHO.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingEcho() throws Exception {
        // Get the echo
        restEchoMockMvc.perform(get("/api/echoes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateEcho() throws Exception {
        // Initialize the database
        echoService.save(echo);

        int databaseSizeBeforeUpdate = echoRepository.findAll().size();

        // Update the echo
        Echo updatedEcho = echoRepository.findOne(echo.getId());
        // Disconnect from session so that the updates on updatedEcho are not directly saved in db
        em.detach(updatedEcho);
        updatedEcho
            .myEcho(UPDATED_MY_ECHO);

        restEchoMockMvc.perform(put("/api/echoes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedEcho)))
            .andExpect(status().isOk());

        // Validate the Echo in the database
        List<Echo> echoList = echoRepository.findAll();
        assertThat(echoList).hasSize(databaseSizeBeforeUpdate);
        Echo testEcho = echoList.get(echoList.size() - 1);
        assertThat(testEcho.getMyEcho()).isEqualTo(UPDATED_MY_ECHO);
    }

    @Test
    @Transactional
    public void updateNonExistingEcho() throws Exception {
        int databaseSizeBeforeUpdate = echoRepository.findAll().size();

        // Create the Echo

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restEchoMockMvc.perform(put("/api/echoes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(echo)))
            .andExpect(status().isCreated());

        // Validate the Echo in the database
        List<Echo> echoList = echoRepository.findAll();
        assertThat(echoList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteEcho() throws Exception {
        // Initialize the database
        echoService.save(echo);

        int databaseSizeBeforeDelete = echoRepository.findAll().size();

        // Get the echo
        restEchoMockMvc.perform(delete("/api/echoes/{id}", echo.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Echo> echoList = echoRepository.findAll();
        assertThat(echoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Echo.class);
        Echo echo1 = new Echo();
        echo1.setId(1L);
        Echo echo2 = new Echo();
        echo2.setId(echo1.getId());
        assertThat(echo1).isEqualTo(echo2);
        echo2.setId(2L);
        assertThat(echo1).isNotEqualTo(echo2);
        echo1.setId(null);
        assertThat(echo1).isNotEqualTo(echo2);
    }
}
