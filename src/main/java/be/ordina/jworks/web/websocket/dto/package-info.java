/**
 * Data Access Objects used by WebSocket services.
 */
package be.ordina.jworks.web.websocket.dto;
