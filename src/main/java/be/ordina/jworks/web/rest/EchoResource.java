package be.ordina.jworks.web.rest;

import com.codahale.metrics.annotation.Timed;
import be.ordina.jworks.domain.Echo;
import be.ordina.jworks.service.EchoService;
import be.ordina.jworks.web.rest.errors.BadRequestAlertException;
import be.ordina.jworks.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Echo.
 */
@RestController
@RequestMapping("/api")
public class EchoResource {

    private final Logger log = LoggerFactory.getLogger(EchoResource.class);

    private static final String ENTITY_NAME = "echo";

    private final EchoService echoService;

    public EchoResource(EchoService echoService) {
        this.echoService = echoService;
    }

    /**
     * POST  /echoes : Create a new echo.
     *
     * @param echo the echo to create
     * @return the ResponseEntity with status 201 (Created) and with body the new echo, or with status 400 (Bad Request) if the echo has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/echoes")
    @Timed
    public ResponseEntity<Echo> createEcho(@RequestBody Echo echo) throws URISyntaxException {
        log.debug("REST request to save Echo : {}", echo);
        if (echo.getId() != null) {
            throw new BadRequestAlertException("A new echo cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Echo result = echoService.save(echo);
        return ResponseEntity.created(new URI("/api/echoes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /echoes : Updates an existing echo.
     *
     * @param echo the echo to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated echo,
     * or with status 400 (Bad Request) if the echo is not valid,
     * or with status 500 (Internal Server Error) if the echo couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/echoes")
    @Timed
    public ResponseEntity<Echo> updateEcho(@RequestBody Echo echo) throws URISyntaxException {
        log.debug("REST request to update Echo : {}", echo);
        if (echo.getId() == null) {
            return createEcho(echo);
        }
        Echo result = echoService.save(echo);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, echo.getId().toString()))
            .body(result);
    }

    /**
     * GET  /echoes : get all the echoes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of echoes in body
     */
    @GetMapping("/echoes")
    @Timed
    public List<Echo> getAllEchoes() {
        log.debug("REST request to get all Echoes");
        return echoService.findAll();
        }

    /**
     * GET  /echoes/:id : get the "id" echo.
     *
     * @param id the id of the echo to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the echo, or with status 404 (Not Found)
     */
    @GetMapping("/echoes/{id}")
    @Timed
    public ResponseEntity<Echo> getEcho(@PathVariable Long id) {
        log.debug("REST request to get Echo : {}", id);
        Echo echo = echoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(echo));
    }

    /**
     * DELETE  /echoes/:id : delete the "id" echo.
     *
     * @param id the id of the echo to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/echoes/{id}")
    @Timed
    public ResponseEntity<Void> deleteEcho(@PathVariable Long id) {
        log.debug("REST request to delete Echo : {}", id);
        echoService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
