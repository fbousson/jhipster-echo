/**
 * View Models used by Spring MVC REST controllers.
 */
package be.ordina.jworks.web.rest.vm;
