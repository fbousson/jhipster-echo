package be.ordina.jworks.service.impl;

import be.ordina.jworks.service.EchoService;
import be.ordina.jworks.domain.Echo;
import be.ordina.jworks.repository.EchoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing Echo.
 */
@Service
@Transactional
public class EchoServiceImpl implements EchoService {

    private final Logger log = LoggerFactory.getLogger(EchoServiceImpl.class);

    private final EchoRepository echoRepository;

    public EchoServiceImpl(EchoRepository echoRepository) {
        this.echoRepository = echoRepository;
    }

    /**
     * Save a echo.
     *
     * @param echo the entity to save
     * @return the persisted entity
     */
    @Override
    public Echo save(Echo echo) {
        log.debug("Request to save Echo : {}", echo);
        return echoRepository.save(echo);
    }

    /**
     * Get all the echoes.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Echo> findAll() {
        log.debug("Request to get all Echoes");
        return echoRepository.findAll();
    }

    /**
     * Get one echo by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Echo findOne(Long id) {
        log.debug("Request to get Echo : {}", id);
        return echoRepository.findOne(id);
    }

    /**
     * Delete the echo by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Echo : {}", id);
        echoRepository.delete(id);
    }
}
