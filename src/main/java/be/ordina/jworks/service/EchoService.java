package be.ordina.jworks.service;

import be.ordina.jworks.domain.Echo;
import java.util.List;

/**
 * Service Interface for managing Echo.
 */
public interface EchoService {

    /**
     * Save a echo.
     *
     * @param echo the entity to save
     * @return the persisted entity
     */
    Echo save(Echo echo);

    /**
     * Get all the echoes.
     *
     * @return the list of entities
     */
    List<Echo> findAll();

    /**
     * Get the "id" echo.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Echo findOne(Long id);

    /**
     * Delete the "id" echo.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
