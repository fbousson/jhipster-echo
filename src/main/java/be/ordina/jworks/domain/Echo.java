package be.ordina.jworks.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Echo.
 */
@Entity
@Table(name = "echo")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Echo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "my_echo")
    private String myEcho;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMyEcho() {
        return myEcho;
    }

    public Echo myEcho(String myEcho) {
        this.myEcho = myEcho;
        return this;
    }

    public void setMyEcho(String myEcho) {
        this.myEcho = myEcho;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Echo echo = (Echo) o;
        if (echo.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), echo.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Echo{" +
            "id=" + getId() +
            ", myEcho='" + getMyEcho() + "'" +
            "}";
    }
}
