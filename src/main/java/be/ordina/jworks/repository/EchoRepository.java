package be.ordina.jworks.repository;

import be.ordina.jworks.domain.Echo;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Echo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EchoRepository extends JpaRepository<Echo, Long> {

}
