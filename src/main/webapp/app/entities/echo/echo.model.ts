import { BaseEntity } from './../../shared';

export class Echo implements BaseEntity {
    constructor(
        public id?: number,
        public myEcho?: string,
    ) {
    }
}
