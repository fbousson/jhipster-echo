import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Echo } from './echo.model';
import { EchoService } from './echo.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-echo',
    templateUrl: './echo.component.html'
})
export class EchoComponent implements OnInit, OnDestroy {
echoes: Echo[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private echoService: EchoService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.echoService.query().subscribe(
            (res: HttpResponse<Echo[]>) => {
                this.echoes = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInEchoes();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Echo) {
        return item.id;
    }
    registerChangeInEchoes() {
        this.eventSubscriber = this.eventManager.subscribe('echoListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
