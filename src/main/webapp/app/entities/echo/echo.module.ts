import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { JhechoSharedModule } from '../../shared';
import {
    EchoService,
    EchoPopupService,
    EchoComponent,
    EchoDetailComponent,
    EchoDialogComponent,
    EchoPopupComponent,
    EchoDeletePopupComponent,
    EchoDeleteDialogComponent,
    echoRoute,
    echoPopupRoute,
} from './';

const ENTITY_STATES = [
    ...echoRoute,
    ...echoPopupRoute,
];

@NgModule({
    imports: [
        JhechoSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        EchoComponent,
        EchoDetailComponent,
        EchoDialogComponent,
        EchoDeleteDialogComponent,
        EchoPopupComponent,
        EchoDeletePopupComponent,
    ],
    entryComponents: [
        EchoComponent,
        EchoDialogComponent,
        EchoPopupComponent,
        EchoDeleteDialogComponent,
        EchoDeletePopupComponent,
    ],
    providers: [
        EchoService,
        EchoPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class JhechoEchoModule {}
