import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Echo } from './echo.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Echo>;

@Injectable()
export class EchoService {

    private resourceUrl =  SERVER_API_URL + 'api/echoes';

    constructor(private http: HttpClient) { }

    create(echo: Echo): Observable<EntityResponseType> {
        const copy = this.convert(echo);
        return this.http.post<Echo>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(echo: Echo): Observable<EntityResponseType> {
        const copy = this.convert(echo);
        return this.http.put<Echo>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Echo>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Echo[]>> {
        const options = createRequestOption(req);
        return this.http.get<Echo[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Echo[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Echo = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Echo[]>): HttpResponse<Echo[]> {
        const jsonResponse: Echo[] = res.body;
        const body: Echo[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Echo.
     */
    private convertItemFromServer(echo: Echo): Echo {
        const copy: Echo = Object.assign({}, echo);
        return copy;
    }

    /**
     * Convert a Echo to a JSON which can be sent to the server.
     */
    private convert(echo: Echo): Echo {
        const copy: Echo = Object.assign({}, echo);
        return copy;
    }
}
