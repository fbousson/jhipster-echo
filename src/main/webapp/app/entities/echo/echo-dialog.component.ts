import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Echo } from './echo.model';
import { EchoPopupService } from './echo-popup.service';
import { EchoService } from './echo.service';

@Component({
    selector: 'jhi-echo-dialog',
    templateUrl: './echo-dialog.component.html'
})
export class EchoDialogComponent implements OnInit {

    echo: Echo;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private echoService: EchoService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.echo.id !== undefined) {
            this.subscribeToSaveResponse(
                this.echoService.update(this.echo));
        } else {
            this.subscribeToSaveResponse(
                this.echoService.create(this.echo));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Echo>>) {
        result.subscribe((res: HttpResponse<Echo>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Echo) {
        this.eventManager.broadcast({ name: 'echoListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-echo-popup',
    template: ''
})
export class EchoPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private echoPopupService: EchoPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.echoPopupService
                    .open(EchoDialogComponent as Component, params['id']);
            } else {
                this.echoPopupService
                    .open(EchoDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
