import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Echo } from './echo.model';
import { EchoPopupService } from './echo-popup.service';
import { EchoService } from './echo.service';

@Component({
    selector: 'jhi-echo-delete-dialog',
    templateUrl: './echo-delete-dialog.component.html'
})
export class EchoDeleteDialogComponent {

    echo: Echo;

    constructor(
        private echoService: EchoService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.echoService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'echoListModification',
                content: 'Deleted an echo'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-echo-delete-popup',
    template: ''
})
export class EchoDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private echoPopupService: EchoPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.echoPopupService
                .open(EchoDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
