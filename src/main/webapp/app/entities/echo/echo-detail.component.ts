import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { Echo } from './echo.model';
import { EchoService } from './echo.service';

@Component({
    selector: 'jhi-echo-detail',
    templateUrl: './echo-detail.component.html'
})
export class EchoDetailComponent implements OnInit, OnDestroy {

    echo: Echo;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private echoService: EchoService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInEchoes();
    }

    load(id) {
        this.echoService.find(id)
            .subscribe((echoResponse: HttpResponse<Echo>) => {
                this.echo = echoResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInEchoes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'echoListModification',
            (response) => this.load(this.echo.id)
        );
    }
}
