import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { EchoComponent } from './echo.component';
import { EchoDetailComponent } from './echo-detail.component';
import { EchoPopupComponent } from './echo-dialog.component';
import { EchoDeletePopupComponent } from './echo-delete-dialog.component';

export const echoRoute: Routes = [
    {
        path: 'echo',
        component: EchoComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jhechoApp.echo.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'echo/:id',
        component: EchoDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jhechoApp.echo.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const echoPopupRoute: Routes = [
    {
        path: 'echo-new',
        component: EchoPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jhechoApp.echo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'echo/:id/edit',
        component: EchoPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jhechoApp.echo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'echo/:id/delete',
        component: EchoDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'jhechoApp.echo.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
